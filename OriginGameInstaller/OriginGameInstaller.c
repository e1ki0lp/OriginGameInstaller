/// OriginGameInstaller.c
/// Origin Game Install and Repair Tool

#include "io.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "windows.h" // windows api
#pragma comment(linker, "/subsystem:windows /entry:mainCRTStartup")

static wchar_t* Error[2] = {
    L"Error",
    L"错误"
};
static wchar_t* ErrorInfo[2] = {
    L"Not Found %s!\r\nPlease make sure run in the game root directory.",
    L"没有发现 %s！\r\n请确保在游戏根目录下运行。"
};

static wchar_t* Success[2] = {
    L"Success",
    L"成功"
};
static wchar_t* SuccessInfo[2] = {
    L" Install Successful !",
    L" 游戏安装成功！"
};

int main(int argc, char** argv)
{
    int languageCode = 1;
    wchar_t path[MAX_PATH];
    wchar_t dir[MAX_PATH];
    wchar_t file[MAX_PATH];
    wchar_t pbdir[MAX_PATH];
    size_t tmp = 0;
    mbstowcs_s(&tmp, path, strlen(argv[0]) + 1, argv[0], MAX_PATH);
    *wcsrchr(path, '\\') = 0;
    wsprintf(dir, L"%s%s", path, L"\\__installer");
    wsprintf(file, L"%s%s", dir, L"\\Touchup.exe");
    wsprintf(pbdir, L"%s%s", dir, L"\\punkbuster");
    if (_waccess(dir, 0) != 0 || _waccess(file, 0) != 0)
    {
        wchar_t error[MAX_PATH];
        wsprintf(error, ErrorInfo[languageCode], file);
        MessageBox(NULL, error, Error[languageCode], MB_OK);
        return 0;
    }

    wchar_t parames[MAX_PATH];
    wsprintf(parames, L"%s%s\"%s\"%s",
        L" install -locale zh_TW -autologging -startmenuIcon=1 -desktopIcon=1 ",
        L" -installPath ", path,
        _waccess(pbdir, 0) == 0 ? L" -Opt:InstallPB " : L"");

    // create new process
    SHELLEXECUTEINFO ShExecInfo = { 0 };
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = file;
    ShExecInfo.lpParameters = parames;
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_SHOW;
    ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);

    wcscat_s(path, wcslen(SuccessInfo[languageCode]) + 1, SuccessInfo[languageCode]);
    MessageBox(NULL, path, Success[languageCode], MB_OK);
    return 0;
}